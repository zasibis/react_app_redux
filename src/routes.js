// import React from 'react';
import {LOGIN_ROUTES, CHAT_ROUTES, USERS_ROUTES} from './components/utils';
import Login from './components/Login';
import Chat from './components/Chat';
import Users from './components/Users';


export const publicRoutes = [
    {
        path: LOGIN_ROUTES,
        Component: Login
    }
]

export const privateRoutes = [
    {
        path: CHAT_ROUTES,
        Component: Chat
    }
]

export const adminRoutes = [
    {
        path: USERS_ROUTES,
        Component: Users
    }
]