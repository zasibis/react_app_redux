export const LOGIN_ROUTES ='/login'
export const CHAT_ROUTES ='/chat'
export const USERS_ROUTES ='/users'
export const USERS_EDIT_ROUTES ='/users/edit/:id'