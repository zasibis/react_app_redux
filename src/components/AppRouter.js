import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import {privateRoutes, pulbicRoutes} from '../routes';
import {CHAT_ROUTES, LOGIN_ROUTES} from './utils/const';

const AppRouter = () => {
    const user = false
    return user ? 
    (
        <Switch>
            {privateRoutes.map(({path, Component}) => 
                <Route path={path} component={Component} exact={true}/>
            )}
            <Redirect to={CHAT_ROUTES} />
        </Switch>
    )
    :
    (
        <Switch>
            {pulbicRoutes.map(({path, Component}) => 
                <Route path={path} component={Component} exact={true}/>
            )}
            <Redirect to={LOGIN_ROUTES} />
        </Switch>
    )
};

export default AppRouter;