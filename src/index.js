import React from "react";
import ReactDOM from "react-dom";
import Chat from "./components/Chat";
import AppRouter from "./components/AppRouter"

ReactDOM.render(
  <React.StrictMode>
    <Chat />
    <AppRouter />
  </React.StrictMode>,
  document.getElementById("root")
);
